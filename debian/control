Source: obs-command-source
Section: video
Priority: optional
Maintainer: Joao Eriberto Mota Filho <eriberto@debian.org>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13), cmake, libobs-dev (>= 29)
Standards-Version: 4.6.2
Homepage: https://obsproject.com/forum/resources/dummy-source-to-execute-command.952/
Vcs-Browser: https://salsa.debian.org/debian/obs-command-source
Vcs-Git: https://salsa.debian.org/debian/obs-command-source.git

Package: obs-command-source
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, obs-studio (>= 29)
Enhances: obs-studio
Breaks: obs-text-slideshow (<= 1.5.2-3)
Description: plugin for OBS Studio providing a dummy source to execute commands
 This plugin provides a dummy source to execute arbitrary commands when a scene
 is switched.
 .
 Current features:
 .
   Start a command at the following events:
     * Show (the source is shown in either preview or program).
     * Hide (the source is hidden so that no longer shown in neither preview
       nor program).
     * Activate (the source goes to the program).
     * Deactivate (the source goes from the program).
     * Show in preview (the source goes to the preview).
     * Hide from preview (the source goes from the preview).
 .
   Optionally, kill the created process at these conditions (this feature is
   not available for Windows as of now):
     * When hiding, kill the process created at shown.
     * When deactivating, kill the process created at activated.
     * When hiding from the preview, kill the process created at preview.
 .
 Possible usage:
   * Implementing custom tally lights.
   * Controlling PTZ cameras by switching the scene. Ii is possible to combine
     with CURL to send some commands.
   * Start and stop a daemon program required for the scene.
   * Trigger other operations through websocket at the event. A helper script
     is available for this (request-websocket.py).
   * Start or stop a streaming and recording.
   * Open a full screen video.
   * Open a calculator to teach about finances when switching a scene.
   * Other actions.
 .
 Caution: since this plugin executes arbitrary commands, users need to consider
 security concerns. For example, when combining with obs-websocket plugin, a
 remote user could change property through the websocket interface so that the
 arbitrary commands can be executed.
